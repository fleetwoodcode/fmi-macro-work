﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FMI_Label_Manager
{
    static class Program
    {
        public static string[] alLabels;
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
