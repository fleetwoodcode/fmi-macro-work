﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net;

namespace FMI_Label_Manager
{
    public partial class Form1 : Form
    {

        public static List<string> loadedlist = new List<string>();
        public static List<string> parsedmacros = new List<string>();
        public static string[] validl;
        public Form1()
        {
   
            try
            {
                InitializeComponent();
                this.listBox1.Items.Add((object)("Connecting to macro server."));
                this.listBox1.Refresh();
                string baseURL = @"http://192.167.6.107/macros/";
                WebClient client = new WebClient();
                string datafiles = client.DownloadString(baseURL);
                MatchCollection matches = Regex.Matches(datafiles, @"(([\w\d])\w+(.MAC))", RegexOptions.IgnoreCase);
                foreach (var match in matches)
                {
                    loadedlist.Add(match.ToString());
                }
            
                parsedmacros = loadedlist.Distinct().ToList();
                this.listBox1.Items.Add("Found " + parsedmacros.Count.ToString() + " macros on macro server.");
                this.listBox1.Items.Add("Ready.");
                this.listBox1.Refresh();

            }
            catch { this.listBox1.Items.Add("Failed to connect."); }

            
        }
            

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }


        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {


            string labelload = this.textBox1.Text.ToUpperInvariant().ToString();
            if (e.KeyData.ToString().Trim() == "Return")
            {
                if (parsedmacros.Contains(labelload + ".mac"))
                {
                    MessageBox.Show("Loaded Label: " + labelload);
                    try
                    {
                       string[] oldmacro = System.IO.Directory.GetFiles(@"C:\\macros");
                        foreach(string oldFile in oldmacro)
                        {
                            System.IO.File.Delete(oldFile);
                        }
                        WebClient macrodata = new WebClient();
                      byte[] macrofile =  macrodata.DownloadData(@"http://192.167.6.107/macros/" + labelload + ".mac");
                        System.IO.File.WriteAllBytes(@"C:\\macros\" + labelload + ".mac", macrofile);
                        byte[] controlmacro = macrodata.DownloadData(@"http://192.167.6.107/macros/AAAAA.mac");
                        System.IO.File.WriteAllBytes(@"C:\\macros\AAAAA.mac", controlmacro);
                        this.label5.Text = this.textBox1.Text.ToUpper();
                        this.label5.Refresh();
                    }
                    catch { MessageBox.Show("Label Load/Validation error"); };


                }
                else { MessageBox.Show("Label Not Found"); }
                
                }

            }
        

   
    }
    }

