﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net;

namespace FMI_Label_Manager
{
    public partial class Form1 : Form
    {
        public static string currentLabel = string.Empty;
        public static List<string> macroslist = new List<string>();
        public static string[] validl;
        public Form1()
        {
   
            try
            {
                InitializeComponent();
                CheckloadedLabel();
                this.listBox1.Items.Add("Found " + Program.alLabels.Count.ToString() + " macros.");
                this.listBox1.Items.Add("Ready.");
                this.listBox1.Refresh();

                    foreach(string macro in FMI_Label_Manager.Program.alLabels)
                  {
                      macroslist.Add(System.IO.Path.GetFileName(macro));
                 }

            }
            catch { this.listBox1.Items.Add("Failed to connect."); }

            
        }
            

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void CheckloadedLabel()
        {
            List<string> currentlab = System.IO.Directory.GetFiles(@"C:\\macros").ToList();
            foreach (string curload in currentlab)
            {
                if(curload != "AAAAA.mac")
                {
                    try
                    {
                        char[] splitc = { '.' };
                        this.label5.Text = System.IO.Path.GetFileName(curload).ToUpperInvariant().Split(splitc)[0];
                        this.label5.Refresh();
                    }
                    catch { }
                }
            }
            
            

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {


            string labelload = this.textBox1.Text.ToUpperInvariant().ToString();
            if (e.KeyData.ToString().Trim() == "Return")
            {

                if (macroslist.Contains(labelload + ".mac"))
                {
                    try
                    {
                       string[] oldmacro = System.IO.Directory.GetFiles(@"C:\\macros");
                        foreach(string oldFile in oldmacro)
                        {
                            System.IO.File.Delete(oldFile);
                        }

                        System.IO.File.Copy(@"C:\\AllMacros\" + labelload + ".mac", @"C:\\macros\" + labelload + ".mac");
                        if(System.IO.File.Exists(@"C:\\AllMacros\AAAAA.mac"))
                            {
                            System.IO.File.Copy(@"C:\\AllMacros\AAAAA.mac", @"C:\\macros\AAAAA.mac");
                            }
                        this.label5.Text = this.textBox1.Text.ToUpper();
                        this.label5.Refresh();
                        this.textBox1.Clear();
                    }
                    catch { MessageBox.Show("Label Load/Validation error"); };


                }
                else { MessageBox.Show("Label Not Found"); }
                
                }

            }
        

        private void label5_Click(object sender, EventArgs e)
        {

        }



        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
    }

