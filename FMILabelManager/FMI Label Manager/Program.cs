﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FMI_Label_Manager
{
    static class Program
    {
        public static List<string> alLabels;
    //    public static Dictionary<string, string> Filematchup
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            alLabels = System.IO.Directory.GetFiles(@"C:\\AllMacros").ToList() ; //path to labels
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
