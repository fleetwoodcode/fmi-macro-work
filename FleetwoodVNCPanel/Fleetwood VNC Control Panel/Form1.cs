﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Net;
using System.Management;
using ListNetworkComputers;
using System.DirectoryServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading;

namespace Fleetwood_IT_Control_Panel
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static bool connectionstatus = false;
        public static string connection = string.Empty;
        public string RetrieveIP(string nameof)
        {
            string ipdata = string.Empty;
            int founddata = -1;
            ProcessStartInfo processStartInfo = new ProcessStartInfo();
                           Process process = new Process();
                           processStartInfo.FileName = "ping";
                           processStartInfo.RedirectStandardInput = false;
                         processStartInfo.RedirectStandardOutput = true;
                         processStartInfo.Arguments = "-4 " + nameof;
                          processStartInfo.UseShellExecute = false;
                          process = Process.Start(processStartInfo);
            while (founddata <= -1)
            {
                founddata = ipdata.Trim().ToLower().IndexOf(":", 0);
                if (founddata > -1) { break; }
                ipdata = process.StandardOutput.ReadLine();
            }
            process.Kill();
            return ipdata;
           

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

    
        private void button1_Click(object sender, EventArgs e)
        {
            NetworkBrowser BrowseSystem = new NetworkBrowser();
           System.Collections.ArrayList Computerlist = BrowseSystem.getNetworkComputers();
            foreach (string pcname in Computerlist)
            {
                this.listBox1.Items.Add(pcname);
            }

        }
        public static string selectedcomp = string.Empty;
        private void button2_Click(object sender, EventArgs e)
        {
            Thread proccesshandler = new Thread(Process_Exited);
            if (!connectionstatus)
            {
                string ip = RetrieveIP(this.listBox1.SelectedItem.ToString());
                Match result = Regex.Match(ip, @"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b");
                ProcessStartInfo processStartInfo = new ProcessStartInfo();
                Process process = new Process();
                processStartInfo.FileName = "vncviewer";
                processStartInfo.RedirectStandardInput = false;
                processStartInfo.RedirectStandardOutput = true;
                processStartInfo.Arguments = result.Value;
                processStartInfo.UseShellExecute = false;
                process = Process.Start(processStartInfo);
                connectionstatus = true;
                proccesshandler.Start();
                
            }
        }

        private static void Process_Exited()
        {
            while (connectionstatus)
            {
                Process[] pname = Process.GetProcessesByName("vncviewer");
                if (pname.Length == 0)
                { connectionstatus = false; MessageBox.Show("Closed Connection"); }
            }
        }

      
        
    }


    }

        
    

